\documentclass[xcolor=table,aspectratio=169,dvipsnames]{beamer}
\usepackage{bm}
\usepackage[utf8]{inputenc}
\usepackage{graphviz}
\usepackage{amsmath}
\usepackage{empheq}
\usepackage{booktabs}
\usepackage{color}
\usepackage{multicol}
\usepackage{siunitx} %pretty measurement unit rendering
\usepackage{multirow} %required for auto-generated tables http://www.tablesgenerator.com/
\usepackage[thinlines]{easytable}
\usepackage{hyperref} %enable hyperlink for urls
\usepackage{soul} %strikethrough via \st{}
\usepackage[autoprint=false, gobble=auto, keeptemps=all, pyfuture=all]{pythontex} % create figures on-line directly from python!

\input{pythontex/functions.py}
\begin{pythontexcustomcode}[begin]{py}
DOC_STYLE = 'slides/main.conf'
pytex.add_dependencies(DOC_STYLE, 'slides/template.conf')
\end{pythontexcustomcode}

\newcommand{\myhfill}{\hskip0pt plus 1filll}

\DeclareUnicodeCharacter{00A0}{ }
\setbeamersize{text margin left=0.8em,text margin right=0.8em}

\DeclareSIUnit\pixel{px}

\usecolortheme[RGB={199,199,199}]{structure}
\usetheme{Dresden}

\newenvironment{figurehere}
{\def\@captype{figure}}
{}
\makeatother

\definecolor{dy}{RGB}{202,202,0}
\definecolor{mg}{gray}{0.30}
\definecolor{lg}{gray}{0.60}
\definecolor{vlg}{gray}{0.75}
\definecolor{tlg}{gray}{0.9}
\definecolor{myred}{HTML}{EF0533}
\definecolor{mybrown}{HTML}{8B4513}

\definecolor{mypurple}{HTML}{8600E9}
\definecolor{mymagenta}{HTML}{D600D6}
\definecolor{mycyan}{HTML}{00A9A9}
\definecolor{mygreen}{HTML}{00Df1F}
\definecolor{myblue}{HTML}{0004F7}
\definecolor{myyellow}{HTML}{CAA200}
\definecolor{myred}{HTML}{E41F00}

\setbeamercolor{caption name}{fg=lg}
\setbeamercolor{caption}{fg=lg}
\setbeamercolor{author}{fg=lg}
\setbeamercolor{institute}{fg=lg}
\setbeamercolor{date}{fg=lg}
\setbeamercolor{title}{fg=mg}
\setbeamertemplate{caption}{\centering\insertcaption\par}
\setbeamertemplate{navigation symbols}{}

\title[Standardization in fMRI Data Processing --- From Data to Software]{Standardization in fMRI Data Processing}
\subtitle{From Data to Software}
\author{Horea-Ioan Ioanas}
\institute{Institute for Biomedical Engineering, ETH and University of Zürich}
\begin{document}
	\begin{frame}
		\titlepage
		\centering\scriptsize [ \href{https://bitbucket.org/TheChymera/education-standardization_fmri}{\texttt{bitbucket.org/TheChymera/education-standardizationi\_fmri}} ]
	\end{frame}
	\section{Relevance}
		\subsection{How this will make you awesome.}
			\begin{frame}{But Why?}
				Transparent, open, and modern standards:
				\begin{itemize}
					\item provide optimal access to the work of others (data \textbf{and} analysis).
					\item make your work optimally accessible (citation \textbf{and} publication).
					\item make your work sustainable (incremental competence).
					\item make your work more transparent (more clarity).
					\item make your work more automatable (higher efficiency).
				\end{itemize}
			\end{frame}
	\section{Meta/Data}
		\subsection{What files to have, and how to document them.}
			\begin{frame}{Provenance}
				Data always comes from somewhere; map it out to visualize data standards.
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/provenance}
				\end{figure}
				\vspace{-1em}
				Standardization vs. what was handed to you:
				\begin{itemize}
					\item The rawest data is the best recourse.
					\item However, your own work is limited (by scope, time, access).
				\end{itemize}
			\end{frame}
			\begin{frame}{Data Files}
				\vspace{-1.3em}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/data}
				\end{figure}
				\vspace{-1.9em}
				\begin{itemize}
					\item Keep your raw data unchanged.
					\item Data save/load support at every step allows:
					\begin{itemize}
						\item resource conservation.
						\item debugging.
						\item collaboration.
					\end{itemize}
				\end{itemize}
			\end{frame}
			\begin{frame}{Metadata}
				= Data about your data (e.g. subject / sequence / treatment)
				\vspace{-1.9em}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/metadata}
				\end{figure}
				\vspace{-2.2em}
				Implicit representation:
				\begin{itemize}
					\item Use existing and documented metadata fields (e.g. headers).
					\item Easily distribute data and relevant documentation.
					\item Understand common metadata fields.
					\item Automate metadata usage in analysis.
				\end{itemize}
			\end{frame}
			\begin{frame}{Metadata --- How Not to Do It:}
				\vspace{-1.4em}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/metadata_bad}
				\end{figure}
				\vspace{-1.7em}
				Ad hoc tracking:
				\begin{itemize}
					\item Increases \textcolor{mybrown}{manual intervention}.
					\item Creates \textcolor{myred}{misleading files}.
					\item Hinders data sharing.
				\end{itemize}
			\end{frame}
	\section{Formats}
		\subsection{Data structures, and directory structures to put them in.}
			\begin{frame}{File Formats}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/formats}
				\end{figure}
				\vspace{-1em}
				Few formats, based on transparency, adoption, openness, losslessness.
				\begin{itemize}
					\item \textcolor{lg}{In all cases:} anything $>$ proprietary
					\item \textcolor{lg}{Volumetric data:} \textbf{NIfTI} $>$ DICOM
					\item \textcolor{lg}{Summary data:} \textbf{CSV} $>$ MAT $>$ XML $>$ XLS
					\item \textcolor{lg}{Graphics:} \textbf{PDF} $>$ PNG $>$ JPG
				\end{itemize}
			\end{frame}
			\begin{frame}{NIfTI --- Neuroimaging Informatics Technology Initiative}
				Modern, widely-understood, lossless, volumetric format, containing:
				\begin{itemize}
					\item Data matrix
					\item Header
				\end{itemize}
				\vspace{.5em}
					\begin{minipage}{.35\textwidth}
						\begin{itemize}
						\item[]\begin{itemize}
							\item Do not delete!
							\item Do not falsify voxel size!
						\end{itemize}
						\end{itemize}
					\end{minipage}%
					\begin{minipage}{.7\textwidth}
						\begin{itemize}
						\item[]\begin{itemize}
							\item If the header breaks the registration, fix the registration!
							\item Meaningful coordinates are obtained from the template.
						\end{itemize}
						\end{itemize}
					\end{minipage}
				\vspace{-1em}
				\py{pytex_fig('scripts/template.py', conf='slides/template.conf', label='pattern_roi')}
			\end{frame}
			\begin{frame}{CSV --- Comma Separated Values}
				Simple, universally understood plain text file.
				\begin{itemize}
					\item A table where rows are rows and columns are delimited by commas.
					\item Equivalent to TSV.
					\item Ideal for:
					\begin{itemize}
						\item ROI data.
						\item global metrics (e.g. SNR, similarity, etc).
						\item automatic query and edit.
					\end{itemize}
				\end{itemize}
			\end{frame}
			\begin{frame}{Directory Tree Format}
				A neat way to embed metadata \textit{and} ease manual overview.
				\begin{itemize}
					\item A standard for naming your files.
					\item A standard for organizing your directories.
				\end{itemize}
				\begin{figure}
					\includegraphics[scale=1.05]{img/dirs.png}
					\caption{A mess vs. BIDS \cite{Gorgolewski2016}}
				\end{figure}
			\end{frame}
			\begin{frame}{BIDS --- Brain Imaging Data Structure \cite{Gorgolewski2016}}
				A peer reviewed way to name your files.
				\begin{itemize}
					\item Open, Collaborative.
					\item Supported as input/output by dozens of applications.
					\item Compatible with OpenfMRI: \href{https://github.com/INCF/openfmri2bids}{\texttt{github.com/INCF/openfmri2bids}}.
				\end{itemize}
			\vspace{1.4em}
			File format (abridged):\\
				\small{\centerline{\texttt{$<$base$>$/\textcolor{myred}{sub-$<$sub$>$}/\textcolor{mygreen}{ses-$<$ses$>$}/func/}}}\\
				\small{\centerline{\texttt{\textcolor{myred}{sub-$<$sub$>$}\_\textcolor{mygreen}{ses-$<$ses$>$}\_\textcolor{myblue}{task-$<$task$>$}\_\textcolor{myyellow}{acq-$<$acq$>$}\_\textcolor{mymagenta}{$<$modality$>$}.nii}}}

			\vspace{1.7em}
			\normalsize Example:\\
				\small{\centerline{\texttt{preprocessed/\textcolor{myred}{sub-5686}/\textcolor{mygreen}{ses-ofM}/func/}}}
				\small{\centerline{\texttt{\textcolor{myred}{sub-5686}\_\textcolor{mygreen}{ses-ofM}\_\textcolor{myblue}{task-CogB}\_\textcolor{myyellow}{acq-seEPI}\_\textcolor{mymagenta}{cbv}.nii}}}
			\end{frame}
			\begin{frame}{BIDS, in SAMRI}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/bids}
				\end{figure}
				\vspace{-1.2em}
				SAMRI apready reposits all data steps BIDS-style.
				\begin{itemize}
					\item If you provide the correct information in the Bruker metadata.
					\item No underscores, dashes, dots, etc. in BIDS values.
				\end{itemize}
			\end{frame}
			\begin{frame}{ParaVision Input}
				Clearly enter metadata in ParaVision --- so that you never have to again:
				\begin{figure}
					\includegraphics[scale=1]{img/ss.png}
					\caption{Appropriate fields for BIDS subject and session values.}
				\end{figure}
			\end{frame}
			\begin{frame}{ParaVision Input}
				Clearly enter metadata in ParaVision --- so that you never have to again:
				\begin{figure}
					\includegraphics[scale=1.33]{img/at.png}
					\caption{Appropriate field for BIDS acquisition, modality, and trial values. Note that the order is flexible.}
				\end{figure}
			\end{frame}
			\begin{frame}{No Data Left Behind}
				Make all old data accessible to automated BIDS-reorganization:
				\begin{itemize}
					\item Bruker metadata = text files.
					\item Metadata can be rapidly adapted to aforementioned format.
					\item Modification is \SI{100}{\percent} non-destructive.
					\item \underline{I'll help you!}
				\end{itemize}
			\end{frame}
	\section{Software}
		\subsection{Data structures, and directory structures to put them in.}
			\begin{frame}{Processing Steps}
				\begin{figure}
					\centering
					\includedot[width=0.98\textwidth]{data/processing}
				\end{figure}
				\begin{itemize}
					\item There is no neuroimaging without the data processing steps.
					\item Data processing steps = Software.
				\end{itemize}
			\end{frame}
			\begin{frame}{Software Usage Inside Processing Steps}
				\begin{minipage}{.51\textwidth}
					\begin{itemize}
						\item Yes, complicated!
						\item Your software should create:
						\begin{itemize}
							\item parallel jobs.
							\item graphic workflow representations.
							\item self-contained directories for each step.
						\end{itemize}
					\end{itemize}
				\end{minipage}
				\begin{minipage}{.48\textwidth}
					\vspace{-3em}
					\begin{figure}
						%\includedot[width=0.85\textwidth]{data/graph}
						\includedot[height=1.1\textheight]{data/graph}
					\end{figure}
				\end{minipage}
			\end{frame}
			\begin{frame}{Workflow Management Software}
				\begin{itemize}
					\item \textcolor{myred}{Bash}:
					\begin{itemize}
						\item Limited features.
						\item Poor parallel jobs management.
						\item Ad-hoc (no workflow standardization).
						\item Unsuitable for analysis.
					\end{itemize}
					\item \textcolor{myred}{MATLAB}:
					\begin{itemize}
						\item Proprietary (access limited and automatic setup impossible).
						\item Poor external process management.
						\item No library for complex workflow management.
					\end{itemize}
					\item \textcolor{mygreen}{Python}:
					\begin{itemize}
						\item Fulfills all requirements on which Bash and MATLAB fail.
						\item Standardized workflow representation via Nipype \cite{nipype}.
						\item \textcolor{mygreen}{Any workflow you have can be represented in Nipype}. \underline{I'll help you!}
					\end{itemize}
				\end{itemize}
			\end{frame}
			\begin{frame}{Software Dependencies}
				\vspace{-2.5em}
				\begin{figure}
					\centering
					\includedot[width=0.63\textwidth]{data/dependencies}
				\end{figure}
				\vspace{-2.2em}
				\begin{itemize}
					\item Program doing the “actual work” may be very deep in the haystack.
					\item Document the immediate requirements of your workflow ...
				\end{itemize}
				\centering ... in a clear and automatically resolvable format (e.g. PMS\cite{pms}).
			\end{frame}
			\begin{frame}{What Dependencies to Use?}
				\centering Freedom is key --- but there are a few useful guidelines:
				\begin{itemize}
					\item Up-to-date-ness (do not use Aedes).
				\end{itemize}
				\begin{figure}
					\includegraphics[width=0.8\textwidth]{img/aedes.png}\\
					\includegraphics[width=0.8\textwidth]{img/afni.png}
				\end{figure}
				\begin{itemize}
					\item Continuous Integration (= reliability).
				\end{itemize}
				\vspace{-.5em}
				\begin{figure}
					\includegraphics[width=0.38\textwidth]{img/afni_.png}\hspace{1em}
					\includegraphics[width=0.38\textwidth]{img/samri.png}
				\end{figure}
			\end{frame}
			\begin{frame}{Contribute to Software Projects}
				\begin{itemize}
					\item Provides quality control.
					\item Encourages standardization and transparency.
					\item Makes your work sustainable --- via an incidental contract:
					\begin{itemize}
						\item \textit{You} make your work understandable and compatible.
						\item \textit{Others} maintain it.
					\end{itemize}
					\item It is more sustainable to fix a “bug” than work around it.
					\item You get credit.
				\end{itemize}
			\end{frame}
			\begin{frame}{The AIC Software Environment}
				\vspace{-2.5em}
				\begin{figure}
					\centering
					\includedot[width=0.77\textwidth]{data/aic}
				\end{figure}
				\vspace{-2em}
			\end{frame}
	\section{References}
		\scriptsize
		\bibliographystyle{unsrt}
		\bibliography{./bib}

\end{document}
